import "../css/normalize.css";
import "../css/style.css";
import "../youtube-modal/index.css";
import "../youtube-modal/index.js";

const KEYS = {
  API_KEY: "AIzaSyBjjndtyUqolEWnSzG4rTVHyVt_AIJ0yQs",
  CLIENT_ID:
    "963453330106-l04qmlgmoltaboebhq9iakj9rh83b6l6.apps.googleusercontent.com",
};
const requestedChannelId = "UC05mHIxrD6IG7dc3c1lEaJg";

const content = document.querySelector(".content");
const subscriptionList = document.querySelector(".nav-menu-subscriptions");
const navMenuMore = document.querySelector(".nav-menu-more");
const showMore = document.querySelector(".show-more");
const searchForm = document.querySelector(".search-form");
const navLinkedLikes = document.querySelectorAll(".nav-link-liked");
const collapseNavBtn = document.querySelector(".menu-button");
const sidebar = document.querySelector(".sidebar");
const navLinkHomes = document.querySelectorAll(".nav-link-home");
const navLinkTrendings = document.querySelectorAll(".nav-link-trending");
const navLinkSubscriptions = document.querySelectorAll(
  ".nav-link-subscriptions"
);
const navLinkMusic = document.querySelector(".nav-link-music");
const navLinkGames = document.querySelector(".nav-link-games");

const createCard = (videoData) => {
  const imgUrl = videoData.snippet.thumbnails.high.url;
  const videoId = videoData.id?.videoId || videoData.id;
  const videoTitle = videoData.snippet.title;
  const publishedAt = videoData.snippet.publishedAt;
  const viewsCount = videoData?.statistics?.viewCount || null;
  const channel = videoData.snippet.channelTitle;

  const card = document.createElement("li");
  card.classList.add("video-card");
  card.innerHTML = `
            <div class="video-thumb">
              <a class="link-video youtube-modal" href="https://www.youtube.com/watch?v=${videoId}">
                <img src="${imgUrl}" alt="" class="thumbnail">
              </a>
            </div>
            <h3 class="video-title">${videoTitle}</h3>
            <div class="video-info">
              <span class="video-counter">
              ${
                viewsCount
                  ? `<span class="video-views">${getViewers(
                      viewsCount
                    )} views</span>`
                  : ""
              }
              <span class="video-date">${getDate(publishedAt)}</span>
              </span>
              <span class="video-channel">${channel}</span>
            </div>
    `;
  return card;
};

const createSubscriptionItem = (channelData) => {
  const imgUrl = channelData.snippet.thumbnails.high.url;
  const channelId = channelData.snippet.resourceId.channelId;
  const channelTitle = channelData.snippet.title;

  const subscriptionItem = document.createElement("li");
  subscriptionItem.classList.add("nav-item");
  /*https://www.youtube.com/channel/${channelId} */
  subscriptionItem.innerHTML = `
    <a href="#" class="nav-link" target="_blank" data-channel-id=${channelId} data-title="${channelTitle}">
      <img src=${imgUrl} alt=${channelTitle} class="nav-image">
      <span class="nav-text">${channelTitle}</span>
    </a>
  `;
  return subscriptionItem;
};

const renderStaticChannelData = (channelData) => {
  const staticChannelAvatar = document.querySelector(".channel-avatar");
  const staticChannelTitle = document.querySelector(".channel-text");

  const avatarUrl = channelData.items[0].snippet.thumbnails.high.url;
  const channelName = channelData.items[0].snippet.title;

  staticChannelAvatar.src = avatarUrl;
  staticChannelTitle.textContent = channelName;
};

const renderList = (videoList, subscriptionData = false, title, clear) => {
  const wrapper = document.createElement("ul");
  const channel = document.createElement("section");
  wrapper.classList.add("video-list");
  channel.classList.add("channel");
  channel.insertAdjacentElement("beforeend", wrapper);

  if (videoList || subscriptionData) {
    if (clear) {
      content.innerHTML = ``;
    }
    if (title) {
      const header = document.createElement("h2");
      header.textContent = title;
      channel.insertAdjacentElement("afterbegin", header);
    }
    if (!subscriptionData) {
      videoList.forEach((video) => {
        const card = createCard(video);
        wrapper.append(card);
      });
      content.insertAdjacentElement("afterbegin", channel);
    } else {
      subscriptionList.textContent = "";
      subscriptionData.forEach((video) => {
        const item = createSubscriptionItem(video);
        subscriptionList.append(item);
      });
    }
  }
};

const getViewers = (viewsCount) => {
  let viewsOnStage = viewsCount.toString().split("");

  if (viewsCount >= 1000 && viewsCount <= 10000) {
    return `${viewsOnStage[0]}.${viewsOnStage[1]}k`;
  }
  if (viewsCount >= 10000 && viewsCount <= 100000) {
    return `${viewsOnStage[0] + viewsOnStage[1]}k`;
  }
  if (viewsCount >= 100000 && viewsCount <= 1000000) {
    return `${viewsOnStage[0] + viewsOnStage[1] + viewsOnStage[2]}k`;
  }
  if (viewsCount >= 1000000 && viewsCount <= 10000000) {
    return `${viewsOnStage[0]}.${viewsOnStage[1]}m`;
  }
  if (viewsCount >= 10000000 && viewsCount <= 1000000000) {
    return `${viewsOnStage[0] + viewsOnStage[1] + viewsOnStage[2]}m`;
  }
};

const getDate = (data) => {
  const currentDay = Date.parse(new Date());
  const days = Math.round((currentDay - Date.parse(new Date(data))) / 86400000);
  if (days > 365) {
    return Math.round(days / 365) + " year(s) ago";
  }
  if (days > 30) {
    if (days > 60) {
      return Math.round(days / 30) + " months ago";
    }
    return "1 month ago";
  }
  if (days > 1) {
    return Math.round(days) + " days ago";
  }

  return "1 day ago";
};

// const getViewers = (count) => {
//   if(count >= 1000000) {
//     return Math.round(count / 1000000) + 'm views'
//   }
//   if(count >= 1000) {
//     return Math.round(count / 1000) + 'k views'
//   }
//     return count + ' views'
// }

// Youtube API

const authBtn = document.querySelector(".auth-btn");
const userAvatar = document.querySelector(".user-avatar");

const handleSuccessAuth = (data) => {
  authBtn.classList.add("hide");
  userAvatar.classList.remove("hide");
  userAvatar.src = data.getImageUrl();
  userAvatar.alt = data.getName();

  requestSubscriptions((data) => {
    renderList(null, data, "Subscriptions");
  });
};

const handleAuthError = (data) => {
  authBtn.classList.remove("hide");
  userAvatar.classList.add("hide");
  userAvatar.src = "";
  userAvatar.alt = "";
};

const handleAuth = () => {
  gapi.auth2.getAuthInstance().signIn();
};

const handleSignOut = () => {
  gapi.auth2.getAuthInstance().signOut();
};

const updateStatusAuth = (data) => {
  data.isSignedIn.listen(() => {
    updateStatusAuth(data);
  });
  if (data.isSignedIn.get()) {
    const userData = data.currentUser.get().getBasicProfile();
    handleSuccessAuth(userData);
  } else {
    handleAuthError();
  }
};

async function initClient() {
  try {
    await gapi.client.init({
      apiKey: KEYS["API_KEY"],
      clientId: KEYS["CLIENT_ID"],
      scope: "https://www.googleapis.com/auth/youtube.readonly",
      discoveryDocs: [
        "https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest",
      ],
    });
    updateStatusAuth(gapi.auth2.getAuthInstance());
    authBtn.addEventListener("click", handleAuth);
    userAvatar.addEventListener("click", handleSignOut);
    // getChannel();
    loadApp();
  } catch (error) {
    console.warn(error);
  }
}

gapi.load("client:auth2", initClient);

const getChannel = () => {
  gapi.client.youtube.channels
    .list({
      part: "snippet, statistics",
      id: requestedChannelId,
    })
    .execute((response) => {
      renderStaticChannelData(response);
    });
};

const requestVideos = (channelId, callback, maxResults = 6) => {
  gapi.client.youtube.search
    .list({
      part: "snippet",
      channelId,
      maxResults,
      order: "date",
    })
    .execute((response) => callback(response.items));
};

const requestTrends = (callback, maxResults = 6) => {
  gapi.client.youtube.videos
    .list({
      part: "snippet, statistics",
      chart: "mostPopular",
      regionCode: "UA",
      maxResults,
    })
    .execute((response) => callback(response.items));
};

const requestMusic = (callback, maxResults = 6) => {
  gapi.client.youtube.videos
    .list({
      part: "snippet, statistics",
      chart: "mostPopular",
      regionCode: "UA",
      maxResults,
      videoCategoryId: "10",
    })
    .execute((response) => callback(response.items));
};

const requestSearch = (searchText, callback, maxResults = 12) => {
  gapi.client.youtube.search
    .list({
      part: "snippet",
      q: searchText,
      maxResults,
      order: "relevance",
    })
    .execute((response) => callback(response.items));
};

const requestSubscriptions = (callback, maxResults = 12) => {
  try {
    gapi.client.youtube.subscriptions
      .list({
        part: "snippet",
        mine: true,
        maxResults,
        order: "relevance",
      })
      .execute((response) => {
        callback(response.items);
      });
  } catch (err) {
    console.log("err: ", err);
  }
};

const requestLikes = (callback, maxResults = 6) => {
  try {
    gapi.client.youtube.videos
      .list({
        part: "snippet, statistics",
        maxResults,
        myRating: "like",
      })
      .execute((response) => {
        callback(response.items);
      });
  } catch (err) {
    console.log("err: ", err);
  }
};

const requestGames = (callback, maxResults = 6) => {
  gapi.client.youtube.videos
    .list({
      part: "snippet, statistics",
      chart: "mostPopular",
      regionCode: "UA",
      maxResults,
      videoCategoryId: "20",
    })
    .execute((response) => callback(response.items));
};

const loadApp = (clear = false) => {
  requestMusic((data) => {
    renderList(data, null, "Music", clear);
    requestTrends((data) => {
      renderList(data, null, "Trends");
      requestVideos(requestedChannelId, (data) => {
        renderList(data, null, "Discovery Russia");
      });
    });
  });
  document.querySelector(".loader")?.classList.add("hidden");
  requestSubscriptions((data) => {
    renderList(null, data, "Subscription");
  });
};

showMore.addEventListener("click", (event) => {
  event.preventDefault();
  navMenuMore.classList.toggle("nav-menu-more-show");
});

searchForm.addEventListener("submit", (event) => {
  event.preventDefault();
  const searchText = searchForm.elements.search.value;
  requestSearch(searchText, (data) => {
    renderList(data, null, "Searching result", true);
  });
});

subscriptionList.addEventListener("click", (event) => {
  event.preventDefault();
  const target = event.target;
  const channelLink = target.closest(".nav-link").dataset.channelId;
  const channelTitle = target.closest(".nav-link").dataset.title;

  if (channelLink) {
    requestVideos(
      channelLink,
      (data) => {
        renderList(data, null, channelTitle, true);
      },
      12
    );
  }
});

navLinkedLikes.forEach((link) => {
  link.addEventListener("click", (event) => {
    event.preventDefault();
    requestLikes((data) => {
      renderList(data, null, "Liked videos", true);
    }, 9);
  });
});

navLinkHomes.forEach((link) => {
  link.addEventListener("click", (event) => {
    event.preventDefault();
    loadApp("clear");
  });
});
navLinkTrendings.forEach((link) => {
  link.addEventListener("click", (event) => {
    event.preventDefault();
    requestTrends((data) => {
      renderList(data, null, "Trends", true);
    });
  });
});
navLinkSubscriptions.forEach((link) => {
  link.addEventListener("click", (event) => {
    event.preventDefault();
  });
});

navLinkMusic.addEventListener("click", (event) => {
  event.preventDefault();
  requestMusic((data) => {
    renderList(data, null, "Music", true);
  }, 9);
});
navLinkGames.addEventListener("click", (event) => {
  event.preventDefault();
  requestGames((data) => {
    renderList(data, null, "Games", true);
  }, 9);
});

collapseNavBtn.addEventListener("click", (event) =>
  sidebar.classList.toggle("collapsed")
);

window.addEventListener("resize", (event) => {
  const width = window.innerWidth;
  if (width < 1359) {
    sidebar.classList.add("collapsed");
  }
});

if (document.body.clientWidth < 1359) sidebar.classList.add("collapsed");
